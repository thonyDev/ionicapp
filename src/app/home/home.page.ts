import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ConexionService } from '../services/conexion.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  information = null;
  nombre = null;

  constructor(private con: ConexionService, public toastController: ToastController) { }
  ngOnInit() {
  }

  consultarUsuario(usuario) {


    if (usuario.length > 3) {
      if (usuario.toUpperCase() === 'JAMAR') {
        this.presentToast('No es valido consultar con la palabra ' + usuario);
      } else {
        this.con.getDetail(usuario).subscribe(result => {
          this.information = result.items.slice(1, 11);
          this.nombre = null;
        });
      }
    } else {

      this.presentToast('El parametro a enviar debe tener minimo 4 caracteres!!!');



    }
  }

  async presentToast(mensaje) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }
}
